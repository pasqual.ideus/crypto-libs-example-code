package tink;

import java.io.IOException;

import java.security.GeneralSecurityException;

import com.google.crypto.tink.Aead;

import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.aead.AeadKeyTemplates;
import com.google.crypto.tink.config.TinkConfig;



public class Tink_Example {

	public static void main(String[] args) throws GeneralSecurityException, IOException {
		
		//Registrierung
		TinkConfig.register();
		
		//Schlüssel wird generiert
		KeysetHandle keysetHandle=KeysetHandle.generateNew(AeadKeyTemplates.AES256_GCM); 
		keysetHandle= KeysetHandle.generateNew(AeadKeyTemplates.AES256_GCM);
		Aead aead =keysetHandle.getPrimitive(Aead.class);
	    
		String plaintext="Was ist die Lieblingsbeschäftigung von Bits? – Busfahren";
		System.out.println("PlainText: "+plaintext);
		String password="12345qwertz";			//Authentifizierung

		
		//Verschlüsselung
		byte[] ciphertext=aead.encrypt(plaintext.getBytes(), password.getBytes());
		String cipherString=new String(ciphertext);
		System.out.println("ChiperText: "+cipherString);

		//Entschlüsselung
		byte[] decrypted= aead.decrypt(ciphertext, password.getBytes());
		String decryptedString=new String(decrypted);
		
		System.out.println("Entschlüsselter ChipherText: "+decryptedString);
		
	}
}
