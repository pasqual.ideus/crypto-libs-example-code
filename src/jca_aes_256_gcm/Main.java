package jca_aes_256_gcm;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;


/**
 * source: https://nullbeans.com/how-to-encrypt-decrypt-files-byte-arrays-in-java-using-aes-gcm/
 * 
 *
 */
public class Main {

	 public static void main(String[] args) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, IOException {
		
		 
		        String file = args[0];
		        String destFile = args[1];
		        String mode = args[2];
		        String password = args[3];
		 
		        byte[] fileBytes = FileManager.readFile(file);
		       
		        byte[] resultBytes = null;
		        if(mode.equals("encrypt")){
		        	resultBytes = Encryption_Decryption.encryptData(password, fileBytes);
		            System.out.println("Erfolgreiche Verschlüsselung des Plainexts. Die verschlüsselte Datei befindet sich: " + destFile );
		        }else {
		        	resultBytes = Encryption_Decryption.decryptData(password, fileBytes);
		            System.out.println("Erfolgreiche Entschlüsselung des Plaintexts. Die entschlüsselte Datei befindet sich hier: " + destFile);
		        }
		 
		        FileManager.writeFile(destFile, resultBytes);
		    }
		}

